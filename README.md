# Telegram bot for chatGPT


- [How to use](#how-to-use)
- [How to deploy](#how-to-deploy)
  - [Locally](#local-start)
  - [Docker](#docker)

## How to use

<strong>`/start`</strong> - New session

<strong>`/dan`</strong> - New session in DAN mode

## How to deploy

### Local start

1. Set the environment variables in `.env` file or set it in bash line:

   1. <em>`CHATGPT_TOKEN`</em> ([View OpenAI API Key in <u>User settings</u>](https://beta.openai.com/account/api-keys)）
   2. <em>`TELEGRAM_BOT_TOKEN`</em> ([Telegram bot token](https://t.me/BotFather))

2. Execute the command

```bash
# install dependencies
pnpm install

# Start the bot service
pnpm run run

# or run by pm2
pnpm run run:pm2
```

### Docker

```bash
# Build image
docker-compose build

# Run Docker with `.env` environment variables file
docker run --env-file .env -d nitroniks:telegram-chatgpt-bot
```
