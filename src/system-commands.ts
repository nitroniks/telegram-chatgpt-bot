import { Markup } from 'telegraf';
import fs from 'fs';
import yaml from 'yaml';
const yamlConfig = fs.readFileSync('./src/system-messages.yml', { encoding: 'utf-8' });
const systemMessages = yaml.parse(yamlConfig);

const initBotCommands = async (bot: any, api: any) => {
    api.setSysMessages(systemMessages);
    for(let command in systemMessages){
        bot.command(command, async (ctx: any) => {
            api.resetConversation(ctx.chat.id.toString(), command);
            ctx.reply(`Новая сессия (${command})`, Markup.removeKeyboard());
        })
    }
}

export {
    initBotCommands
};