import 'es6-promise'
import 'fetch-everywhere'
import { Markup, Telegraf } from 'telegraf';
import { env } from './utils/env';
import { ChatGPT } from './chatgpt';
import { initBotCommands } from './system-commands';

const api = new ChatGPT({
  apiKeys: [ env.CHATGPT_TOKEN ],
});
const processUsers: Set<Number> = new Set([]);

function escapeHtml(unsafe:String)
{
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
 }

const bot = new Telegraf(env.TELEGRAM_BOT_TOKEN, {
  handlerTimeout: 3 * 60 * 1000,
});

bot.start(async (ctx) => {
  ctx.reply('Новая сессия', Markup.removeKeyboard());
  api.resetConversation(ctx.chat.id.toString(), 'start');
});

initBotCommands(bot, api);

bot.on('text', ctx => {
  if(processUsers.has(ctx.chat.id))
  {
    ctx.reply('🔥Одновременно только один запрос')
    return
  }
  (async () => {
    const text = ctx.message?.text.trim();
    const telegramUserId = ctx.from?.id;

    const removeKeyboard = Markup.removeKeyboard();

    switch (text) {
      default:
        await ctx.sendChatAction('typing');
        try {
          const message = await ctx.sendMessage('Думою...');
          processUsers.add(ctx.chat.id);
          let gptAnswer = await api.sendMessage(text, ctx.chat.id.toString());
          processUsers.delete(ctx.chat.id);
          if(!gptAnswer)
            return;

          gptAnswer = escapeHtml(gptAnswer);
          gptAnswer = gptAnswer.replace(/```([\s\S]*?)$([\s\S]+?)```/gm, '<pre language="$1">$2</pre>')

          await Promise.all([
            ctx.telegram.deleteMessage(message.chat.id, message.message_id),
            bot.telegram.sendMessage(ctx.chat.id, gptAnswer, { parse_mode: 'HTML' }),
          ]);
        } catch (e: any) {
          if(ctx.chat && ctx.chat.id)
            processUsers.delete(ctx.chat.id);
          await ctx.sendMessage(
            '❌Something went wrong. Details: ' + e.message,
            removeKeyboard,
          );
        }
    }
  })();
});

bot.catch(console.error);
bot.launch().then(console.log).catch(console.error);
console.log('Bot started');
