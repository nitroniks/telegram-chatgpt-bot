import { get_encoding } from '@dqbd/tiktoken'
import { unwatchFile } from 'fs';
// const GPT_API = 'http://localhost:3004/chat/completions';
const GPT_API = 'https://api.openai.com/v1/chat/completions';
const GPT_MODEL = 'gpt-3.5-turbo-0301';

const DEFAULT_TEMPLATE = {
    model: GPT_MODEL,
    temperature: 0.5,
    top_p: 1,
    presence_penalty: 1,
    stream: false
};

const DEFAULT_CONFIG = {
    maxModelTokens: 4000,
    maxResponseTokens: 1000
}

const USER_LABEL_DEFAULT = 'User'
const ASSISTANT_LABEL_DEFAULT = 'ChatGPT'


const tokenizer = get_encoding('cl100k_base')

interface ChatGPTOptions {
    apiKeys: Array<String>
}

interface Message{
    role: String,
    content: string
}

interface Conversation{
    id: String,
    mode: string,
    messages: Array<Message>
}

interface BuildMessageOpts {
    mode?: string,
    conversationId: String
}

interface ResponseGPT{
    text: string,
    content: string
}

interface SystemMessageList {
    [key: string]: string
}

class ChatGPT {
    apiKeys: Array<String>
    conversations: Array<Conversation>
    sysMessages: SystemMessageList
    
    constructor({ apiKeys }: ChatGPTOptions) {
        this.apiKeys = apiKeys
        this.conversations = []
        this.sysMessages = { }
    }

    setSysMessages(sysMessages: SystemMessageList){
        this.sysMessages = { ...this.sysMessages, ...sysMessages };
    }

    resetConversation(conversationId: String, mode: string){
        let con = this.conversations.find(v => v.id == conversationId);
        if(!mode)
            mode = 'start';
        if(!con) {
            this.conversations.push({ id: conversationId, mode, messages: [] })
            con = this.conversations[this.conversations.length - 1]
        }else{
            con.messages = [];
            con.mode = mode
        }
    }

    async sendMessage(text: string, conversationId: String) : Promise<string | undefined>{
        if(!conversationId) return;

        let con = this.conversations.find(v => v.id == conversationId);
        if(!con) {
            this.conversations.push({ id: conversationId, mode: 'start', messages: [] })
            con = this.conversations[this.conversations.length - 1]
        }

        const { messages, maxTokens, numTokens, messagePassed } = await this._buildMessages(text, con)

        const body = {
            max_tokens: maxTokens,
            ...DEFAULT_TEMPLATE,
            messages
        };

        if(messagePassed)
        {
            con.messages.push({
                role: 'user',
                content: messages[messages.length - 1].content
            });
        }
        
        const _r = await fetch(GPT_API, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.apiKeys[0]}`
            },
            body: JSON.stringify(body)
        })
        const response = await _r.json();
        let textResult = '';
        if (response?.choices?.length) {
            const message = response.choices[0].message
            textResult = message.content
        } else {
            const res = response as any
            throw new Error(
                `OpenAI error: ${
                    res?.detail?.message || res?.detail || res?.error?.message || res?.error || 'unknown'
                }`
            )
        }
        if(textResult && textResult.length){
            con.messages.push({
                role: 'assistant',
                content: textResult
            });
        }

        return textResult;
    }

    async _buildMessages(text: string, converstion: Conversation){
        const maxNumTokens = DEFAULT_CONFIG.maxModelTokens - DEFAULT_CONFIG.maxResponseTokens
        const userLabel = USER_LABEL_DEFAULT
        const assistantLabel = ASSISTANT_LABEL_DEFAULT;
        let messages: Array<Message> = [];
        let messagePassed = false;

        let sysMessage = converstion.mode ? this.sysMessages[converstion.mode] : this.sysMessages.start;

        if(sysMessage){
            const currentDate = new Date().toISOString().split('T')[0]
            sysMessage = sysMessage.replace(/\$\{DATE\}/gm, currentDate);
            messages.push({
                role: 'system',
                content: sysMessage
            })
        }

        let nextMessages: Array<Message> = text ? messages.concat([
            {
                role: 'user',
                content: text
            }
        ]) : messages
        let numTokens = 0
        let i = converstion.messages.length;

        do{
            const prompt = messages.reduce((prompt, message) => {
                switch (message.role) {
                    case 'system':
                        return prompt.concat([`Instructions:\n${message.content}`])
                    case 'user':
                        return prompt.concat([`${userLabel}:\n${message.content}`])
                    default:
                        return prompt.concat([`${assistantLabel}:\n${message.content}`])
                }
            }, [] as string[]).join('\n\n')

            const nextNumTokensEstimate = await this._getTokenCount(prompt)
            const isValidPrompt = nextNumTokensEstimate <= maxNumTokens
            
            if (prompt && !isValidPrompt) {
                break
            }

            messages = nextMessages
            numTokens = nextNumTokensEstimate

            if(text && !messagePassed){
                messagePassed = true;
            }

            const msg = converstion.messages[i];
            if(msg){
                const parentMessageRole = msg.role || 'user'
                nextMessages.splice(1, 0, { role: parentMessageRole, content: msg.content });
            }
            i--;
        }while(i >= -1)
        const maxTokens = Math.max(
            1,
            Math.min(DEFAULT_CONFIG.maxModelTokens - numTokens, DEFAULT_CONFIG.maxResponseTokens)
        )

        return { messages, maxTokens, numTokens, messagePassed }
    }

    protected async _getTokenCount(text: string) {
        text = text.replace(/<\|endoftext\|>/g, '')
        return tokenizer.encode(text).length
    }
}

export { ChatGPT }