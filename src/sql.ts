export {};
/*
import sqlite3 from "sqlite3";
import { env } from './utils/env';

const sql = sqlite3.verbose()
const db = new sql.Database(env.SQLITE_DB)

interface SessionRecord {
    gpt_last_message: String,
    chat_id: Number,
    dan_mode: Boolean
}

const createTable = async () => {
    db.run(`CREATE TABLE sessions (chat_id integer PRIMARY KEY, gpt_last_message varchar(38), dan_mode bool NOT NULL DEFAULT FALSE)`)
};

const updateSession = async (chatId: Number, gptMessageId: String) => {
    db.run(`UPDATE sessions SET gpt_last_message = ? WHERE chat_id = ?`, [gptMessageId, chatId]);
}

const getSession = (chatId: Number) : Promise<SessionRecord | null> => {
    return new Promise((resolve, reject) => {
            db.get(`SELECT * FROM sessions WHERE chat_id = ?`, [chatId], (err, row : SessionRecord) => {
            if (err) {
                reject(err)
            }
            resolve(row ? row : null)
        });
    })
}

const newSession = async (chatId: Number, danMode?: Boolean) => {
    db.run(`INSERT OR REPLACE INTO sessions(chat_id, gpt_last_message, dan_mode) VALUES (?, NULL, ?)`, [chatId, danMode])
}

const clearSession = async (chatId: Number) => {
    db.run(`DELETE FROM sessions WHERE chat_id = ?`, [chatId])
}

const setDanMode = async (chatId: Number, danMode: Boolean) => {
    db.run(`UPDATE sessions SET dan_mode = ? WHERE chat_id = ?`, [danMode, chatId])
}

export {
    createTable,
    updateSession,
    getSession,
    clearSession,
    newSession,
    setDanMode
};
*/